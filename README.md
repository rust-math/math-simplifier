# math-simplifier

A simple library that lets you simplify linear mathematic expressions and solve linear equations.

Useful together with [math-parser](https://gitlab.com/jD91mZM2/math-parser).
