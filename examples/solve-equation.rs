use math_parser::{tokenizer::Operation, types::*};
use math_simplifier::Simplified;
use std::io::{self, prelude::*};

fn main() -> Result<(), failure::Error> {
    let stdin = io::stdin();
    let mut stdin = stdin.lock();

    let mut inputs = [None, None];

    for i in 0..inputs.len() {
        print!("Input {}: ", i+1);
        io::stdout().flush()?;

        let mut line = String::new();
        stdin.read_line(&mut line)?;

        let ast = math_parser::parse(&line).to_result()?;

        let equation = match OperationNode::cast(ast.root().value()) {
            Some(ref node) if node.operation() == Operation::Equal => *node,
            _ => {
                eprintln!("Syntax: left = right");
                return Ok(());
            }
        };

        let left = Simplified::new(equation.left())?;
        let right = Simplified::new(equation.right())?;

        inputs[i] = Some((left, right));
    }

    print!("Variable to work with: ");
    io::stdout().flush()?;

    let mut var = String::new();
    stdin.read_line(&mut var)?;
    let var = Some(var.trim().to_string());

    let mut extracted = [None, None];

    for (i, input) in inputs.iter_mut().enumerate() {
        let (mut left, mut right) = input.take().unwrap();

        if !left.extract(&mut right, &var) {
            eprintln!("impossible to extract input #{}", i+1);
            return Ok(());
        }
        println!("{} = {}", left, right);

        extracted[i] = Some(right);
    }

    print!("Variable to extract: ");
    io::stdout().flush()?;

    let mut var = String::new();
    stdin.read_line(&mut var)?;
    let var = Some(var.trim().to_string());

    let (mut left, mut right) = (extracted[0].take().unwrap(), extracted[1].take().unwrap());

    println!("{} = {}", left, right);
    if !left.extract(&mut right, &var) {
        eprintln!("impossible to extract from merged inputs");
        return Ok(());
    }

    println!("{} = {}", left, right);

    Ok(())
}
