use math_parser::{tokenizer::Operation, types::*};
use math_simplifier::Simplified;
use std::io::{self, prelude::*};

fn main() -> Result<(), failure::Error> {
    let stdin = io::stdin();
    let mut stdin = stdin.lock();

    print!("Input: ");
    io::stdout().flush()?;

    let mut line = String::new();
    stdin.read_line(&mut line)?;

    let ast = math_parser::parse(&line).to_result()?;

    let equation = match OperationNode::cast(ast.root().value()) {
        Some(ref node) if node.operation() == Operation::Equal => *node,
        _ => {
            eprintln!("Syntax: left = right");
            return Ok(());
        }
    };

    let mut left = Simplified::new(equation.left())?;
    let mut right = Simplified::new(equation.right())?;

    print!("Variable to extract: ");
    io::stdout().flush()?;

    let mut var = String::new();
    stdin.read_line(&mut var)?;
    let var = Some(var.trim().to_string());

    if !left.extract(&mut right, &var) {
        eprintln!("impossible to extract input");
        return Ok(());
    }

    println!("{} = {}", left, right);
    Ok(())
}
