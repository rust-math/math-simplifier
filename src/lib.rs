#[macro_use]
extern crate failure;

use num_traits::*;
use math_parser::{
    parser::{Error as ParseError, Node, NodeType, Types},
    tokenizer::Operation,
    types::*
};
use std::{
    collections::HashMap,
    cmp,
    fmt,
    mem,
    ops::{Add, AddAssign, Sub, SubAssign, Mul, MulAssign, Div, DivAssign}
};

type Fraction = math_types::Fraction<i64>;

/// An error that can occur during simplifying
#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "division by zero")]
    DivideByZero,
    #[fail(display = "expression is non-linear because of the operation {:?}", _0)]
    NonLinearOperation(Operation),
    #[fail(display = "expression is non-linear because of the unary operation {:?}", _0)]
    NonLinearUnary(UnaryOperation),
    #[fail(display = "expression is non-linear because of the unary operation {:?}", _0)]
    NonLinearNode(NodeType),
    #[fail(display = "expression is non-linear because you multiply or divide by a variable")]
    NonLinearUsage,
    #[fail(display = "integer overflow")]
    IntegerOverflow,
    #[fail(display = "integer parsing error: {}", _0)]
    ParseInt(#[cause] std::num::ParseIntError),
    #[fail(display = "fraction (float) parsing error: {}", _0)]
    ParseFraction(#[cause] math_parser::types::ParseFractionError),
    #[fail(display = "parse error: {}", _0)]
    ParseError(#[cause] ParseError)
}

/// A data type that stores linear expressions in a simplified form. Any
/// operation on this preserves the simplified form meaning you never need to
/// re-simplify this. Behind the scenes, this is implemented by simply counting
/// the coefficients of each variable, and using a null variable for offset.
#[derive(Default)]
pub struct Simplified {
    coefficients: HashMap<Option<String>, Fraction>
}
impl Simplified {
    /// Loads the parsed expression into simplified form.
    /// This only works on linear expressions.
    ///
    /// # Panics
    /// If the AST is missing a field due to it being errnous. You should
    /// check this first.
    // This is probably very inefficient due to the recursion and all hashmapping
    // TODO: rewrite efficiently
    pub fn new(expr: Node<rowan::RefRoot<Types>>) -> Result<Self, Error> {
        if let Some(op) = OperationNode::cast(expr) {
            let mut left = Self::new(op.left())?;
            let right = Self::new(op.right())?;

            match op.operation() {
                Operation::Add => Ok(left + right),
                Operation::Sub => Ok(left - right),
                Operation::Mul => {
                    if let Some(left) = left.to_value() {
                        Ok(right * left)
                    } else if let Some(right) = right.to_value() {
                        Ok(left * right)
                    } else {
                        Err(Error::NonLinearUsage)
                    }
                },
                Operation::Div => {
                    if let Some(right) = right.to_value() {
                        if right.is_zero() {
                            return Err(Error::DivideByZero)
                        }
                        Ok(left / right)
                    } else {
                        left.try_divide(&right).map(|_| left)
                    }
                },
                op => Err(Error::NonLinearOperation(op))
            }
        } else if let Some(unary) = Unary::cast(expr) {
            let value = Self::new(unary.value())?;

            match unary.operation() {
                UnaryOperation::Negate => Ok(value * -1),
                op => Err(Error::NonLinearUnary(op))
            }
        } else if let Some(paren) = Paren::cast(expr) {
            Self::new(paren.value())
        } else if let Some(root) = Root::cast(expr) {
            Self::new(root.value())
        } else if let Some(ident) = Ident::cast(expr) {
            let mut map = HashMap::new();
            map.insert(Some(ident.as_str().to_string()), Fraction::from(1));
            Ok(Self {
                coefficients: map
            })
        } else if let Some(value) = Value::cast(expr) {
            let mut map = HashMap::new();
            if let Some(result) = value.to_int() {
                let value = result.map_err(Error::ParseInt)?;
                if value < i64::min_value() as i64 || value > i64::max_value() as i64 {
                    return Err(Error::IntegerOverflow);
                }
                let value = Fraction::from(value);
                map.insert(None, value);
            } else if let Some(result) = value.to_fraction() {
                let value = result.map_err(Error::ParseFraction)?;
                map.insert(None, value);
            } else {
                panic!("invalid ast");
            }
            Ok(Self {
                coefficients: map
            })
        } else {
            Err(Error::NonLinearNode(expr.kind()))
        }
    }
    /// Parses an expression using math_parser and loads it into simplified
    /// form
    pub fn parse(expr: &str) -> Result<Self, Error> {
        Self::new(
            math_parser::parse(expr).to_result()
                .map_err(Error::ParseError)?
                .into_node()
                .borrowed()
        )
    }
    /// If this expression is a single predetermined value, return it.
    /// Otherwise, return None.
    pub fn to_value(&self) -> Option<Fraction> {
        if self.coefficients.len() != 1 {
            return None;
        }
        self.coefficients.get(&None).map(|v| *v)
    }
    /// Get the coefficient of the specified variable
    pub fn coefficient_of(&self, var: &Option<String>) -> Fraction {
        self.coefficients.get(var).map(|v| *v).unwrap_or(Fraction::from(0))
    }
    /// Get a mutable reference to the coefficient of the specified variable
    pub fn coefficient_of_mut(&mut self, var: Option<String>) -> &mut Fraction {
        self.coefficients.entry(var).or_insert(Fraction::from(0))
    }
    /// Divides this expression with another, but only if possible to do
    /// linearly. 5x/2x is fine (because that's actually 5/2), but not 5x/2y.
    /// Returns an error and leaves self unchanged if unsuccessful.
    // TODO: Consider: Since this still doesn't support 5*x*y/2*x*y, it might
    // not be worth keeping...
    pub fn try_divide(&mut self, other: &Simplified) -> Result<(), Error> {
        if self.coefficients.len() != 1 || other.coefficients.len() != 1 {
            return Err(Error::NonLinearUsage);
        }
        for (key, &denominator) in &other.coefficients {
            if denominator.is_zero() {
                return Err(Error::DivideByZero);
            }
            match self.coefficients.get(key) {
                Some(&numerator) => {
                    self.coefficients.remove(key);
                    self.coefficients.insert(None, numerator / denominator);
                },
                None => return Err(Error::NonLinearUsage)
            }
        }
        Ok(())
    }
    /// Move everything except for a specified variable into dest. Leaves self
    /// containing exactly one of that variable. Extracting x from `x/2` with
    /// the dest `x-5` becomes `x=10`.
    ///
    /// This function returns false if the extraction is impossible, such as
    /// `x = x`. The state of both sides then is undefined.
    pub fn extract(&mut self, dest: &mut Simplified, keep: &Option<String>) -> bool {
        // Reset self to only have one of the specified variable
        // x = ...
        let mut new = Self::default();
        new.coefficients.insert(keep.clone(), Fraction::from(1));

        // Move all values to destination
        // `6x = 2x + y` => `0 = -4x + y`
        *dest -= mem::replace(self, new);

        // Move variable back, and divide it so we only have exactly one of them.
        // `0 = -4x + y` => `4x = y` => `x = 0.25y`
        let coefficient = -dest.coefficient_of(keep);
        if coefficient.is_zero() {
            // Impossible, such as `x = x`
            return false;
        }

        dest.coefficients.remove(keep);
        *dest /= coefficient;
        true
    }
}
impl AddAssign<Simplified> for Simplified {
    fn add_assign(&mut self, other: Simplified) {
        for (var, value) in other.coefficients {
            *self.coefficient_of_mut(var) += value;
        }
    }
}
impl SubAssign<Simplified> for Simplified {
    fn sub_assign(&mut self, other: Simplified) {
        for (var, value) in other.coefficients {
            *self.coefficient_of_mut(var) -= value;
        }
    }
}
impl<T: Into<Fraction>> AddAssign<T> for Simplified {
    fn add_assign(&mut self, value: T) {
        *self.coefficient_of_mut(None) += value;
    }
}
impl<T: Into<Fraction>> SubAssign<T> for Simplified {
    fn sub_assign(&mut self, value: T) {
        *self.coefficient_of_mut(None) -= value;
    }
}
impl<T: Into<Fraction>> MulAssign<T> for Simplified {
    fn mul_assign(&mut self, value: T) {
        let value = value.into();
        for coefficient in self.coefficients.values_mut() {
            *coefficient *= value;
        }
    }
}
impl<T: Into<Fraction>> DivAssign<T> for Simplified {
    fn div_assign(&mut self, value: T) {
        *self *= value.into().inv();
    }
}
impl fmt::Display for Simplified {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut entries: Vec<_> = self.coefficients.iter().collect();
        entries.sort_unstable_by_key(|&(var, value)| (cmp::Reverse(value), var));
        let mut first = true;
        for (var, &value) in entries {
            let mut value = value;
            if value.is_zero() {
                continue;
            }
            if !first {
                if !f.alternate() && value < Fraction::from(0) {
                    write!(f, " - ")?;
                    value = -value;
                } else {
                    write!(f, " + ")?;
                }
            }
            let mut print_denominator = false;
            if f.alternate() || var.is_none() || !value.abs().is_one() {
                if (value.as_decimal() * 1000.0 % 1.0).abs() < std::f64::EPSILON {
                    write!(f, "{}", value.as_decimal())?;
                } else {
                    value = value.simplify();
                    if *value.numerator() == -1 {
                        write!(f, "-")?;
                    } else if var.is_none() || *value.numerator() != 1 {
                        write!(f, "{}", value.numerator())?;
                    }
                    print_denominator = true;
                }
            } else if (-value).is_one() {
                write!(f, "-")?;
            }
            first = false;
            if let Some(var) = var {
                write!(f, "{}", var)?;
            }
            if print_denominator {
                write!(f, "/{}", value.denominator())?;
            }
        }
        if first {
            // Either empty or all zeros
            write!(f, "0")?;
        }
        Ok(())
    }
}
impl fmt::Debug for Simplified {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

macro_rules! impl_op {
    ($($op_assign:ident => $op_trait:ident $op_fn:ident $op:tt),*) => {
        $(
        impl<T> $op_trait<T> for Simplified
            where Self: $op_assign<T>
        {
            type Output = Self;

            fn $op_fn(mut self, other: T) -> Self::Output {
                self $op other;
                self
            }
        }
        )*
    }
}
impl_op! {
    AddAssign => Add add +=,
    SubAssign => Sub sub -=,
    MulAssign => Mul mul *=,
    DivAssign => Div div /=
}

#[cfg(test)]
mod tests {
    use super::*;

    fn simplify(math: &str) -> Result<String, Error> {
        Simplified::parse(math).map(|o| o.to_string())
    }
    fn extract(left: &str, right: &str, var: &str) -> Option<(String, String)> {
        let mut left = Simplified::parse(left).unwrap();
        let mut right = Simplified::parse(right).unwrap();

        if left.extract(&mut right, &Some(var.to_string())) {
            Some((left.to_string(), right.to_string()))
        } else {
            None
        }
    }

    #[test]
    fn simplification() {
        assert_eq!(simplify("(1 + 3)x + 3x * 4 + 2").unwrap(), "16x + 2");
        assert_eq!(simplify("-(x - y)").unwrap(), "y - x");
        assert_eq!(simplify("5y + 5x").unwrap(), "5x + 5y");
        assert_eq!(simplify("5x + 6y").unwrap(), "6y + 5x");
        assert_eq!(format!("{:#}", Simplified::parse("-(x - y)").unwrap()), "1y + -1x");
        assert_eq!(simplify("5x/2").unwrap(), "2.5x");
        assert_eq!(simplify("5x/2x").unwrap(), "2.5");
        assert!(simplify("x/0").is_err());
        assert!(simplify("x*x").is_err());
    }

    #[test]
    fn extraction() {
        assert_eq!(extract("5x", "5x", "x"), None);
        assert_eq!(extract("5", "5", "x"), None);

        assert_eq!(extract("x - y", "0", "x"), Some(("x".into(), "y".into())));
        assert_eq!(extract("16x + 2", "50", "x"), Some(("x".into(), "3".into())));
        assert_eq!(extract("x / 0.05", "80", "x"), Some(("x".into(), "4".into())));
        assert_eq!(extract("x + y", "x", "y"), Some(("y".into(), "0".into())));
        assert_eq!(extract("16x", "15x", "x"), Some(("x".into(), "0".into())));
        assert_eq!(extract("5x", "6x", "x"), Some(("x".into(), "0".into())));
        assert_eq!(extract("x + y", "z", "x"), Some(("x".into(), "z - y".into())));
        assert_eq!(extract("x + y + z", "0", "x"), Some(("x".into(), "-y - z".into())));
        assert_eq!(extract("2x", "55x/2", "x"), Some(("x".into(), "0".into())));
        assert_eq!(extract("5x", "6 + 3(2x + 1)", "x"), Some(("x".into(), "-9".into())));
        assert_eq!(extract("x/2", "x-5", "x"), Some(("x".into(), "10".into())));
        assert_eq!(extract("6x", "2x + y", "x"), Some(("x".into(), "0.25y".into())));
    }

    #[test]
    fn print() {
        assert_eq!(simplify("x/3").unwrap(), "x/3");
        assert_eq!(simplify("2x*(1/3)").unwrap(), "2x/3");
        assert_eq!(simplify("x/4").unwrap(), "0.25x");
        assert_eq!(simplify("-x/4").unwrap(), "-0.25x");
        assert_eq!(simplify("-x/3").unwrap(), "-x/3");
    }
}
